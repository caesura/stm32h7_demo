/**********************************************************************
	使用测试外设代码的话，首先在main.h中包含该外设的xx.h，
	然后在main函数中调用函数即可；

	对于w25qxx的测试，三种代码不要混用，根据功能选一个使用;
	对于qspi的bk1和bk2，不能同时使用，所以只能使用一种；
**********************************************************************/
#include "main.h"

int main(void)
{
	Cache_Enable();                 //打开L1-Cache
	HAL_Init();				        //初始化HAL库
	Stm32_Clock_Init(160,5,2,4);    //设置时钟,400Mhz 
	
	uart_init(115200);				//串口1初始化				
	led_init();

	qspi_bk1_init();
	qspi_bk1_test();
	HAL_Delay(1);
	qspi_bk1_test();
	qspi_bk1_deinit();

	qspi_bk2_init();
	qspi_bk2_test();
	HAL_Delay(1);
	qspi_bk2_test();
	qspi_bk2_deinit();

	
	while(1)
	{
		led_test();
		
	}
}

