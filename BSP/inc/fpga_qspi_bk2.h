#ifndef _FPGA_QSPI_BK2_H
#define _FPGA_QSPI_BK2_H
#include <string.h>
#include "stm32h7xx_hal.h"
#include "usart.h"

#define QSPI_BK2_DUMMY_NONE_CYCLE  0
#define QSPI_BK2_DUMMY_2_CYCLE  2

void qspi_bk2_init(void);	
void qspi_bk2_test(void);	
void qspi_bk2_deinit(void);


#endif

