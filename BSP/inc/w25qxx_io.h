#ifndef __W25QXX_IO_H
#define __W25QXX_IO_H
#include <string.h>		//使用memset函数

#include "stm32h7xx_hal.h"
#include "usart.h" 

void iospi_init(void);	
void iospi_test(void);	

//void SPI_DELAY(uint16_t del);
//uint8_t SPI_WRITE_READ_BYTE(uint8_t TX_DAT);

//void w25qxx_io_readID(void);
//void w25qxx_io_eraseSector( u16 sector);
//void w25qxx_io_writePage(u8 *buff,u8 len,u32 address);
//void w25qxx_io_readData(u8 *buff,u32 len,u32 address);
//void w25qxx_io_writeEnable(void);
//void w25qxx_io_waitBusy(void);

#define _SPI_DELAY_		5		//数值太小可能造成通讯异常，当读不到数据时，可以加大这个数值
#define _SPI_CPOL   	0		//选择CLK在空闲时的电平状态，0：空闲时为低电平，1：空闲时为高电平
#define _SPI_CPHA   	0		//选择SPI数据在第几个边沿采集数据，0：在第一个边沿采集数据，1：在第二个边沿采集数据
#define SPI_CS(x)       (x ?  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_SET):HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET) )       
#define SPI_CLK(x)      (x ?  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,GPIO_PIN_SET):HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,GPIO_PIN_RESET) )       
#define SPI_MOSI(x)     (x ?  HAL_GPIO_WritePin(GPIOD,GPIO_PIN_7,GPIO_PIN_SET):HAL_GPIO_WritePin(GPIOD,GPIO_PIN_7,GPIO_PIN_RESET) )       
#define SPI_MISO         HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_4)     
#define SPI_HOLD(x)		(x ?  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_SET):HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_RESET) )

#define W25Q128	0XEF17
#define W25Q256 0XEF18
#define dummy					0xff
#define W25X_WriteEnable		0x06 
#define W25X_WriteDisable		0x04 
#define W25X_ReadStatusReg1		0x05 
#define W25X_ReadStatusReg2		0x35 
#define W25X_ReadStatusReg3		0x15 
#define W25X_WriteStatusReg1    0x01 
#define W25X_WriteStatusReg2    0x31 
#define W25X_WriteStatusReg3    0x11 
#define W25X_ReadData			0x03 
#define W25X_FastReadData		0x0B 
#define W25X_FastReadDual		0x3B 
#define W25X_PageProgram		0x02 
#define W25X_BlockErase			0xD8 
#define W25X_SectorErase		0x20 
#define W25X_ChipErase			0xC7 
#define W25X_PowerDown			0xB9 
#define W25X_ReleasePowerDown	0xAB 
#define W25X_DeviceID			0xAB 
#define W25X_ManufactDeviceID	0x90 
#define W25X_JedecDeviceID		0x9F 
#define W25X_Enable4ByteAddr    0xB7
#define W25X_Exit4ByteAddr      0xE9
#define W25X_SetReadParam		0xC0 
#define W25X_EnterQPIMode       0x38
#define W25X_ExitQPIMode        0xFF

#endif




