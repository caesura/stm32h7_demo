
/**********************************************************************
	适用mmgcu板
	
	short int为2bytes，有符号区别，也就是有正有负；
	unsigned short int也为2bytes，没有符号区别，全部非负，表示范围2^16 bit;

	fmc_data[15:0]连接到单片机；16bit数据宽度使用内部总线stm_addr[25:1]寻址mem_addr[24:0]；
	mem_addr[23:16]连接到单片机，内部总线stm_addr对应应该为[24:17];
	
	keil中老是忘记要把xx.c和xx.h文件加载到工程中，然后编译找不到;
**********************************************************************/
#ifndef _FMC_H_
#define _FMC_H_
#include "stm32h7xx_hal.h"
//#include "stm32h7xx_hal_nor.h"
#include <string.h>
#include "usart.h"


#define FPGA_WRITE(offset,data)	*((volatile unsigned short int *)(0x60000000 + (offset << 17))) = data
#define FPGA_READ(offset)	    *((volatile unsigned short int *)(0x60000000 + (offset << 17)))


void fmc_nor_init(void);
void fmc_nor_test(void);


#endif
