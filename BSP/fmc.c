
/**********************************************************************
*** 适用mmgcu板
*** 需要fmc外设测试时，在main.h中包含进fmc.h文件，
*** 然后调用fmc_nor_init()和fmc_nor_test()就可以直接使用了;
***
*** HAL_NOR_DeInit(&hnor);的作用是验证即将配置的结构体不是随机值；
*** 
*** keil中老是忘记要把xx.c和xx.h文件加载到工程中，然后编译找不到;
**********************************************************************/
#include "fmc.h"

GPIO_InitTypeDef GPIO_InitStruct;
NOR_HandleTypeDef hnor; 
FMC_NORSRAM_TimingTypeDef Timing;

void HAL_NOR_MspInit(NOR_HandleTypeDef *hnor)
{
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();
		
	GPIO_InitStruct.Pin=GPIO_PIN_7;
	GPIO_InitStruct.Mode=GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull=GPIO_NOPULL;
	GPIO_InitStruct.Speed=GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate=GPIO_AF12_FMC;
	HAL_GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	GPIO_InitStruct.Pin=GPIO_PIN_6|GPIO_PIN_7;
	GPIO_InitStruct.Alternate=GPIO_AF9_FMC;
	HAL_GPIO_Init(GPIOC,&GPIO_InitStruct);

	GPIO_InitStruct.Pin=GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|
						GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|
						GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Alternate=GPIO_AF12_FMC;
	HAL_GPIO_Init(GPIOD,&GPIO_InitStruct);
		
	GPIO_InitStruct.Pin=GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_10|
                        GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_11| 
                        GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	HAL_GPIO_Init(GPIOE,&GPIO_InitStruct);
	printf("fmc nor gpio init done...\r\n");	

}


void fmc_nor_init(void)
{
	HAL_NOR_DeInit(&hnor);		
	__HAL_RCC_FMC_CLK_ENABLE();	//开启FMC外设时钟;
	
	//nor 时序配置
	//NADV高电平 = AddressSetupTime + AddressHoldTime;
	//NADV低电平 = DataSetupTime + 1hclk时钟周期; 
	//时钟单位为AHB_HCLK: RCC_HCLK_DIV4;100MHz;
	Timing.AddressSetupTime=3; //地址建立时间n*HCLK 
	Timing.AddressHoldTime=4;  //地址保持时间n*HCLK,同步nor flash中未使用
	Timing.DataSetupTime=5;    //数据建立时间n*HCLK 
	//Timing.BusTurnAroundDuration=0; 		//读写转换插入时间周期为0
	//Timing.CLKDivision=0;          		//FMC输出时钟分频系数 在异步操作时无效
	//Timing.DataLatency=0;           		//数据延迟 在异步操作时无效
	//Timing.AccessMode=FMC_ACCESS_MODE_A; 	//时序操作模式A 在FMC_BCRx寄存器的ExtMode位为0时此设置无效
	
	//nor 参数配置
	hnor.Instance=FMC_NORSRAM_DEVICE;                       //存储器选择
	hnor.Extended=FMC_NORSRAM_EXTENDED_DEVICE;  			//扩展寄存器地址 读写可不同时序
	
	hnor.Init.NSBank=FMC_NORSRAM_BANK1;					    //选择FMC SRAM Bank1区
	hnor.Init.DataAddressMux=FMC_DATA_ADDRESS_MUX_DISABLE;  //使能数据地址不复用
	hnor.Init.MemoryType=FMC_MEMORY_TYPE_NOR; 				//选择存储器类型 
	hnor.Init.MemoryDataWidth=FMC_NORSRAM_MEM_BUS_WIDTH_16; //数据位宽为16位
	hnor.Init.BurstAccessMode=FMC_BURST_ACCESS_MODE_DISABLE;//禁止读突发访问 读操作工作在异步模式

	hnor.Init.WaitSignal=FMC_WAIT_SIGNAL_DISABLE;
	hnor.Init.WaitSignalPolarity=FMC_WAIT_SIGNAL_POLARITY_LOW;
	hnor.Init.WaitSignalActive=FMC_WAIT_TIMING_BEFORE_WS;
	hnor.Init.AsynchronousWait=FMC_ASYNCHRONOUS_WAIT_DISABLE;
	hnor.Init.WriteOperation=FMC_WRITE_OPERATION_ENABLE;

	hnor.Init.ExtendedMode=FMC_EXTENDED_MODE_DISABLE;       //禁止扩展模式  此时读写按照模式2进行
	hnor.Init.WriteBurst=FMC_WRITE_BURST_DISABLE;           //禁止写突发访问 写操作工作在异步模式
	//hnor.Init.ContinuousClock=FMC_CONTINUOUS_CLOCK_SYNC_ASYNC;
	hnor.Init.WriteFifo=FMC_WRITE_FIFO_ENABLE;				//读写数据应该底层是按位操作先的，然后再字节操作；所以要fifo；
	hnor.Init.PageSize=FMC_PAGE_SIZE_NONE;
	
	if(HAL_NOR_Init(&hnor,&Timing,NULL)!=HAL_OK)
		printf("hal nor ram init fail......\r\n");
	else
		printf("hal nor ram init success......\r\n");
}


#define len 8
void fmc_nor_test(void)
{
	int i;
	u8 read[len];
	memset(read,0,sizeof(len));
	for(i=0; i<len; i++)
	{
		FPGA_WRITE(i,i);
	}
	for(i = 0;i < 1000; i++);//延时约2us;
	for(i=0;i<len;i++){
		read[i] = FPGA_READ(i);
	}
	for(i=0;i<len;i++)
		printf("%02x.  ",read[i]);
	printf("\r\n");	
}


