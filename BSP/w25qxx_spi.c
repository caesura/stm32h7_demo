/********************************************************************************
*** 适用mmgcu板
*** 需要spi1外设测试时，在main.h中包含进w25qxx_spi.h文件，
***然后调用spi1_init()和spi1_test()就可以直接使用了;
***
***hold高电平不使能；不配置偶尔会误使能，导致flash无效 
*** 延时函数可以使用HAL库自带的HAL_Delay();
*** keil中老是忘记要把xx.c和xx.h文件加载到工程中，然后编译找不到;
********************************************************************************/
#include "w25qxx_spi.h"

SPI_HandleTypeDef struct_spi ;
SPI_HandleTypeDef *pspi1 = &struct_spi;
//全局变量默认会先初始化成0x00；
//局部变量随机分配存储空间，此时储存空间可能不是0x00；

void HAL_SPI_MspInit(SPI_HandleTypeDef * pspi1)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_SPI1_CLK_ENABLE();

	GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_5;//CS,HOLD
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP; 
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4;//SCK,MISO
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_7;//MOSI
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_SET);//hold高电平不使能；不配置偶尔会误使能，导致flash无效 

	printf("spi1 gpio init done...\r\n");
}

void spi1_init(void)
{
	pspi1->Instance = SPI1;
	pspi1->Init.Mode = SPI_MODE_MASTER;
	pspi1->Init.Direction = SPI_DIRECTION_2LINES;
	pspi1->Init.DataSize = SPI_DATASIZE_8BIT;
	pspi1->Init.CLKPolarity = SPI_POLARITY_LOW;
	pspi1->Init.CLKPhase = SPI_PHASE_1EDGE;
	pspi1->Init.NSS = SPI_NSS_SOFT;
	pspi1->Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	pspi1->Init.FirstBit = SPI_FIRSTBIT_MSB;
	//pspi1->Init.TIMode = SPI_TIMODE_DISABLE;
	//pspi1->Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	//pspi1->Init.CRCPolynomial = 0x7;
	//pspi1->Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
	pspi1->Init.NSSPolarity = SPI_NSS_POLARITY_LOW;
	pspi1->Init.FifoThreshold = SPI_FIFO_THRESHOLD_01DATA;
	//pspi1->Init.TxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
	//pspi1->Init.RxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
	//pspi1->Init.MasterSSIdleness = SPI_MASTER_SS_IDLENESS_00CYCLE;
	//pspi1->Init.MasterInterDataIdleness = SPI_MASTER_INTERDATA_IDLENESS_00CYCLE;
	//pspi1->Init.MasterReceiverAutoSusp = SPI_MASTER_RX_AUTOSUSP_DISABLE;
	//pspi1->Init.MasterKeepIOState = SPI_MASTER_KEEP_IO_STATE_DISABLE;
	//pspi1->Init.IOSwap = SPI_IO_SWAP_DISABLE;
	if (HAL_SPI_Init(pspi1) != HAL_OK)
	{
		printf("spi1 init fail...\r\n");
	}
	else
		printf("spi1 init done...\r\n");
}

void w25qxx_spi1_writeEnable(void)
{
	u8 sendbuff[1]={W25X_WriteEnable};
	u8 revbuff[1]={0xff};
	u16 len =1;
	u32 timeout = 5000;
	SPI1_CS(0);
	HAL_SPI_TransmitReceive(pspi1,sendbuff,revbuff,len,timeout);
	SPI1_CS(1);
	printf("spi1 write enable done...\r\n");
}

void w25qxx_spi1_waitBusy(void)
{
	u8 sendbuff[1]={W25X_ReadStatusReg1};
	u8 revbuff[1]={0xff};
	u8 dummybuff[1]={0xff};
	u16 len =1;
	u32 timeout =5000;
	SPI1_CS(0);
	do{
		HAL_SPI_TransmitReceive(pspi1,sendbuff,revbuff,len,timeout);
		HAL_SPI_TransmitReceive(pspi1,dummybuff,revbuff,len,timeout);
	}while(revbuff[0]&0x01);
	SPI1_CS(1);
	printf("spi1 wait busy done...\r\n");
}

void w25qxx_spi1_readID(void)
{
	u16 id=0;
	u8 sendbuff[8]={W25X_ManufactDeviceID,dummy,dummy,0x00,dummy,dummy};
	u8 revbuff[8];
	u16 len = 6;
	u32 timeout = 5000;
	memset(revbuff,dummy,sizeof(revbuff));
	
	SPI1_CS(0);
	HAL_SPI_TransmitReceive(pspi1,sendbuff,revbuff,len,timeout);
	SPI1_CS(1);

	id=revbuff[4]<<8;
	id=id|revbuff[5];
	printf("spi1 w25qxx id:%4x  \r\n",id);
}

/**一共有4096个sector,每个sector为4096Bytes;************************************
***寻址范围为4096*4096=2^24线；寻址的最小单位是bytes；所以存储容量为2^24*8bit;
***erase sector的寻址参数是sector；
***write page的寻址参数是以Byte为单位的地址；每次最多写入256字节；
***read data的寻址参数也是以Byte为单位的地址，可以一次读完芯片******************/
void w25qxx_spi1_eraseSector( u16 sector)
{
	u32 address = sector*4096;
	u8 sendbuff[4];
	u8 revbuff[4]={dummy,dummy,dummy,dummy};
	u16 len =4;
	u32 timeout =5000;
	
	u8 *psend = sendbuff;
	 *psend = W25X_SectorErase;
	 *(psend+1) = address>>16;
	 *(psend+2) = address>>8;
	 *(psend+3) = address;

	w25qxx_spi1_writeEnable( );
	w25qxx_spi1_waitBusy();
	SPI1_CS(0);
	HAL_SPI_TransmitReceive(pspi1,sendbuff,revbuff,len,timeout);
	SPI1_CS(1);
	w25qxx_spi1_waitBusy();
	printf("spi1 erase sector %#x done...\r\n",sector);
}

void w25qxx_spi1_writePage(u8 *buff,u8 len,u32 address)
{
	u8 sendbuff[4];
	u8 revbuff[4]={dummy,dummy,dummy,dummy};
	u16 sendlen =4;
	u32 timeout =5000;
	u8 dummybuff[256];
	
	u8 *psend = sendbuff;
	 *psend = W25X_PageProgram;
	 *(psend+1) = address>>16;
	 *(psend+2) = address>>8;
	 *(psend+3) = address;
	memset(dummybuff,dummy,sizeof(dummybuff));
	 
	w25qxx_spi1_writeEnable( );
	w25qxx_spi1_waitBusy();
	SPI1_CS(0);
	HAL_SPI_TransmitReceive(pspi1,sendbuff,revbuff,sendlen,timeout);
	HAL_SPI_TransmitReceive(pspi1,buff,dummybuff,len,timeout);
	SPI1_CS(1);
	w25qxx_spi1_waitBusy();
	printf("spi1 write page(addr%#x) done...\r\n",address);
}

void w25qxx_spi1_readData(u8 *buff,u32 len,u32 address)
{
	u8 sendbuff[4];
	u8 revbuff[4]={dummy,dummy,dummy,dummy};
	u16 sendlen =4;
	u32 timeout =5000;
	u8 dummybuff[256];

	u8 *psend = sendbuff;
	 *psend = W25X_ReadData;
	 *(psend+1) = address>>16;
	 *(psend+2) = address>>8;
	 *(psend+3) = address;
	memset(dummybuff,dummy,sizeof(dummybuff));
	
	SPI1_CS(0);
	HAL_SPI_TransmitReceive(pspi1,sendbuff,revbuff,sendlen,timeout);
	HAL_SPI_TransmitReceive(pspi1,dummybuff,buff,len,timeout);
	SPI1_CS(1);
	printf("spi1 read data(addr%#x) done...\r\n",address);
}

void spi1_test(void)
{
	u8 sendbuff[16];
	u8 revbuff[16];
	int i;
	
	w25qxx_spi1_readID();
	
	memset(revbuff,0,sizeof(revbuff));
	for(i=0;i<16;i++)
		sendbuff[i]=i;
		
	w25qxx_spi1_eraseSector(0);
	w25qxx_spi1_writePage(sendbuff,16,0);
	w25qxx_spi1_readData(revbuff,16,0);
	for(i=0;i<16;i++)
		printf("revbuff[%2d]:%2x \r\n",i,revbuff[i]);
}

