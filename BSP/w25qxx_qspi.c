/********************************************************************************
*** 适用laser板
*** 需要qspi_bk1外设测试时，在main.h中包含进w25qxx_qspi.h文件，
*** 然后调用qspi_bk1_init()和qspi_bk1_test()就可以直接使用了;
***
*** 收发默认timeout为5000(5s); 改小了一点为1000(1s);
*** (reg1_status&&0x01) 运算结果恒等于真，所以在wait busy的时候使用&

	本来是使用HAL_QSPI_MspInit()，让qspi初始化的时候自己调用的，
	但是后来代码改成了一个qspi分时复用成两个qspi来使用；
	这样的话要么初始化时同时初始化bk1和bk2，要么不使用回调函数分别初始化；
	根据个人使用习惯，本工程不使用回调函数，然后分别初始化gpio;

*** keil中老是忘记要把xx.c和xx.h文件加载到工程中，然后编译找不到;
********************************************************************************/
#include "w25qxx_qspi.h"

QSPI_HandleTypeDef QSPI_Handler; 


//void HAL_QSPI_MspInit(QSPI_HandleTypeDef *hqspi)
void qspi_bk1_mspinit(QSPI_HandleTypeDef *hqspi)
{
	GPIO_InitTypeDef GPIO_Initure;
	__HAL_RCC_QSPI_CLK_ENABLE(); 
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE(); 
	__HAL_RCC_GPIOE_CLK_ENABLE(); 
	//PB2 CLK; PB10 CS;
    GPIO_Initure.Pin=GPIO_PIN_2|GPIO_PIN_10;
    GPIO_Initure.Mode=GPIO_MODE_AF_PP;         
    GPIO_Initure.Pull=GPIO_PULLUP;              
    GPIO_Initure.Speed=GPIO_SPEED_FREQ_VERY_HIGH; 
    GPIO_Initure.Alternate=GPIO_AF9_QUADSPI;   
    HAL_GPIO_Init(GPIOB,&GPIO_Initure);
	
	//PD11 DI; PD12 DO; PD13 HOLD;
	GPIO_Initure.Pin=GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13;
	HAL_GPIO_Init(GPIOD,&GPIO_Initure);

	//PE2 WP;
	GPIO_Initure.Pin=GPIO_PIN_2;
	HAL_GPIO_Init(GPIOE,&GPIO_Initure);
	
	printf("qspi_bk1  gpio init done\r\n");	
}

void qspi_bk1_init(void)
{
	qspi_bk1_mspinit(&QSPI_Handler);
	
    QSPI_Handler.Instance=QUADSPI;                          //QSPI
    QSPI_Handler.Init.ClockPrescaler=1;                     //QSPI分频比，频率为100M，200/(1+1)=100MHZ
    QSPI_Handler.Init.FifoThreshold=32;                      //FIFO阈值为4个字节??
    QSPI_Handler.Init.SampleShifting=QSPI_SAMPLE_SHIFTING_HALFCYCLE;//采样移位半个周期(DDR模式下,必须设置为0)??
    QSPI_Handler.Init.FlashSize=15;  						//W25Q128大小为16M字节
    QSPI_Handler.Init.ChipSelectHighTime=QSPI_CS_HIGH_TIME_5_CYCLE;//片选高电平时间为5个时钟(10*5=55ns),即手册里面的tSHSL参数
    QSPI_Handler.Init.ClockMode=QSPI_CLOCK_MODE_0;          //模式0
    QSPI_Handler.Init.FlashID=QSPI_FLASH_ID_1;              //第一片flash
    QSPI_Handler.Init.DualFlash=QSPI_DUALFLASH_DISABLE;     //禁止双闪存模式
    if(HAL_QSPI_Init(&QSPI_Handler)==HAL_OK)
		printf("qspi_bk1 init done...\r\n");
    else
		printf("qspi_bk1 init fail...\r\n"); 
}
						//格式：发送指令 + 发送地址 + 			   dummy周期数 +       指令发送模式      +       地址发送模式 +          地址位数 +        数据模式
void QSPI_Send_CMD(u32 instruction,u32 address,u32 dummyCycles,u32 instructionMode,u32 addressMode,u32 addressSize,u32 dataMode)
{
    QSPI_CommandTypeDef Cmdhandler={0};
    
    Cmdhandler.Instruction=instruction;                 	//待发指令
    Cmdhandler.Address=address;                            	//待发地址
    Cmdhandler.DummyCycles=dummyCycles;                     //待发dummy周期数(dummy是无意义周期，并不是空周期)
    Cmdhandler.InstructionMode=instructionMode;				//指令模式，也可以配置成不发送指令字节
    Cmdhandler.AddressMode=addressMode;   					//地址模式
    Cmdhandler.AddressSize=addressSize;   					//地址模式，24bit 32bit等
    Cmdhandler.DataMode=dataMode;             				//数据模式
    Cmdhandler.SIOOMode=QSPI_SIOO_INST_EVERY_CMD;       	//每次都发送指令！！
    Cmdhandler.AlternateByteMode=QSPI_ALTERNATE_BYTES_NONE; //无交替字节
    Cmdhandler.DdrMode=QSPI_DDR_MODE_DISABLE;           	//关闭DDR模式
    Cmdhandler.DdrHoldHalfCycle=QSPI_DDR_HHC_ANALOG_DELAY;

    if(HAL_QSPI_Command(&QSPI_Handler,&Cmdhandler,1000)==HAL_OK)
		;//printf("qspi send command done...\r\n");
	else
		printf("qspi send command fail...\r\n");
}

void QSPI_Receive(u8* buf,u32 datalen)
{
    QSPI_Handler.Instance->DLR=datalen-1;     
    if(HAL_QSPI_Receive(&QSPI_Handler,buf,1000)==HAL_OK) 
		;//printf("qspi rev done...\r\n");
    else 
		printf("qspi rev fail...\r\n");
}

void QSPI_Transmit(u8* buf,u32 datalen)
{
    QSPI_Handler.Instance->DLR=datalen-1;    
    if(HAL_QSPI_Transmit(&QSPI_Handler,buf,1000)==HAL_OK) 
		;//printf("qspi transmit done...\r\n");
    else 
		printf("qspi transmit fail...\r\n");
}

void w25qxx_writeEnable(void)
{
	QSPI_Send_CMD(W25X_WriteEnable,0,0,QSPI_INSTRUCTION_4_LINES,QSPI_ADDRESS_NONE,QSPI_ADDRESS_8_BITS,QSPI_DATA_NONE);	
	printf("qspi_bk1 write enable done...\r\n");
}

void w25qxx_waitBusy(void)
{
	u8 reg1_status;
	do{
		QSPI_Send_CMD(W25X_ReadStatusReg1,0,0,QSPI_INSTRUCTION_4_LINES,QSPI_ADDRESS_NONE,QSPI_ADDRESS_8_BITS,QSPI_DATA_4_LINES);	
		QSPI_Receive(&reg1_status,1);
	}while( reg1_status&0x01 );
	printf("qspi_bk1 wait busy done...\r\n");
}

/*************************************************************************************************
	读sr2，查看bit1是否置1；
	如果读出sr为0x02,表示已经使能qspi模式；
	如果未使能，则先配置sr2，然后发送38h指令;
	
	QSPI_DATA_NONE不需要配合数据使用,QSPI_Send_CMD()本身即可发送指令;像QSPI_DATA_1_LINE这样的就需要配合数据使用；
*************************************************************************************************/
void w25qxx_qspi1_enable(void)
{
	u8 reg2_status;
	QSPI_Send_CMD(W25X_ReadStatusReg2,0,0,QSPI_INSTRUCTION_4_LINES,QSPI_ADDRESS_NONE,QSPI_ADDRESS_8_BITS,QSPI_DATA_4_LINES);
	QSPI_Receive(&reg2_status,1);
	printf("sr2:%#x ;",reg2_status);
	
	if( !(reg2_status&0X02) )
	{
		reg2_status|=1<<1;
		QSPI_Send_CMD(W25X_WriteStatusReg2,0,0,QSPI_INSTRUCTION_1_LINE,QSPI_ADDRESS_NONE,QSPI_ADDRESS_8_BITS,QSPI_DATA_1_LINE);
		QSPI_Transmit(&reg2_status,1);

		QSPI_Send_CMD(W25X_EnterQPIMode,0,0,QSPI_INSTRUCTION_1_LINE,QSPI_ADDRESS_NONE,QSPI_ADDRESS_8_BITS,QSPI_DATA_NONE);
		printf("enable qspi mode \r\n");	
	}
}

void w25qxx_readID(void)
{
	u8 temp[2];
	u16 deviceid;
	QSPI_Send_CMD(W25X_ManufactDeviceID,0,0,QSPI_INSTRUCTION_4_LINES,QSPI_ADDRESS_4_LINES,QSPI_ADDRESS_24_BITS,QSPI_DATA_4_LINES);
	QSPI_Receive(temp,2);
	deviceid=(temp[0]<<8)|temp[1];
	printf("qspi1 id:%4x\r\n",deviceid);
}

void w25qxx_eraseSector( u16 sector)
{	  
 	u32 address = sector*4096;
    w25qxx_writeEnable();              
    w25qxx_waitBusy();  
	QSPI_Send_CMD(W25X_SectorErase,address,0,QSPI_INSTRUCTION_4_LINES,QSPI_ADDRESS_4_LINES,QSPI_ADDRESS_24_BITS,QSPI_DATA_NONE);
    w25qxx_waitBusy();   				   
}

void w25qxx_writePage(u8 *buff,u8 len,u32 address)
{
	w25qxx_writeEnable();					//写使能
	QSPI_Send_CMD(W25X_PageProgram,address,0,QSPI_INSTRUCTION_4_LINES,QSPI_ADDRESS_4_LINES,QSPI_ADDRESS_24_BITS,QSPI_DATA_4_LINES);	
	QSPI_Transmit(buff,len);	         	      
	w25qxx_waitBusy();					   //等待写入结束
}

void w25qxx_readData(u8 *buff,u32 len,u32 address)
{
	QSPI_Send_CMD(W25X_FastReadData,address,2,QSPI_INSTRUCTION_4_LINES,QSPI_ADDRESS_4_LINES,QSPI_ADDRESS_24_BITS,QSPI_DATA_4_LINES);
	QSPI_Receive(buff,len); 

}

void qspi_bk1_deinit(void)
{
	HAL_QSPI_DeInit(&QSPI_Handler);
	printf("qspi_bk1 deinit done.\r\n");
}

/**一共有4096个sector,每个sector为4096Bytes;************************************
***寻址范围为4096*4096=2^24线；寻址的最小单位是bytes；所以存储容量为2^24*8bit;
***erase sector的寻址参数是sector；
***write page的寻址参数是以Byte为单位的地址；每次最多写入256字节；
***read data的寻址参数也是以Byte为单位的地址，可以一次读完芯片******************/
void qspi_bk1_test(void)
{
	u8 sendbuff[16];
	u8 revbuff[16];
	int i;

	w25qxx_qspi1_enable();
	w25qxx_readID();
	
	memset(revbuff,0,sizeof(revbuff));
	for(i=0;i<16;i++)
		sendbuff[i]=i;
		
	w25qxx_eraseSector(2);
	w25qxx_writePage(sendbuff,16,4096*2);
	w25qxx_readData(revbuff,16,4096*2);
	for(i=0;i<16;i++)
		printf("qspi_bk1 revbuff[%2d]:%2x \r\n",i,revbuff[i]);
}


