/********************************************************************************
***适用mmgcu板
*** 需要io模拟spi时，在main.h中包含进w25qxx_io.h文件，
***然后调用iospi_init()和iospi_test()就可以直接使用了;
***
***hold高电平不使能；不配置偶尔会误使能，导致flash无效 
*** __NOP(); 为一个机器周期的延时函数；
*** keil中老是忘记要把xx.c和xx.h文件加载到工程中，然后编译找不到;
********************************************************************************/
#include "w25qxx_io.h"

void iospi_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    __HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
    
    //PB3:SCK、PB6:/CS, PB5:/HOLD;
    GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_6|GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    
    //PB4:MISO 
    GPIO_InitStruct.Pin = GPIO_PIN_4;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);   

	//PD7:MOSI
	GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	SPI_HOLD(1); 			//hold高电平不使能；不配置偶尔会误使能，导致flash无效   
    SPI_CS(1);				//cs高电平不使能

	#if (_SPI_CPOL == 0)    
    	SPI_CLK(0);			//根据CPOL配置时钟起始电平	    
	#else
    	SPI_CLK(1);   
	#endif	
}

void SPI_DELAY(uint16_t del)
{
	uint8_t i;
	for(i=0;i<del;i++)
	{
        __NOP();    
        __NOP();
        __NOP();
        __NOP();
        __NOP();
    }
}

/***函数说明    ：通过_SPI_CPOL和_SPI_CPHA宏来区分SPI 4种不同的通信模式*********/         
#if (_SPI_CPOL == 0)&&(_SPI_CPHA == 0)
uint8_t SPI_WRITE_READ_BYTE(uint8_t TX_DAT)
{
    uint8_t i;
    uint8_t RX_DAT = 0;        
    for(i = 0;i < 8;i ++)
    {
        SPI_CLK(0);
        
        if(TX_DAT & 0x80)
        {
            SPI_MOSI(1);    
        }
        else
        {
            SPI_MOSI(0);       
        }     
        TX_DAT <<= 1;    
        
        SPI_DELAY(_SPI_DELAY_);
                
        SPI_CLK(1);
        
        RX_DAT <<= 1;
    
        if(SPI_MISO != 0)
        {
            RX_DAT |= 0x01;    
        }   
		
        SPI_DELAY(_SPI_DELAY_);			
    }
    
    SPI_CLK(0);//空闲时SCK为低电平
    
    return RX_DAT;
}
#elif (_SPI_CPOL == 0)&&(_SPI_CPHA == 1) 
uint8_t SPI_WRITE_READ_BYTE(uint8_t TX_DAT)
{
    uint8_t i;
    uint8_t RX_DAT = 0;    
       
    for(i = 0;i < 8;i ++)
    {
        SPI_CLK(1);
        
        if(TX_DAT & 0x80)
        {
            SPI_MOSI(1);    
        }
        else
        {
            SPI_MOSI(0);       
        }    

        TX_DAT <<= 1;    
        
        SPI_DELAY(_SPI_DELAY_);
        
        SPI_CLK(0);

        RX_DAT <<= 1;
    
        if(SPI_MISO != 0)
        {
            RX_DAT |= 0x01;    
        }
		
        SPI_DELAY(_SPI_DELAY_);		
		
    }
    
    return RX_DAT;
}
#elif (_SPI_CPOL == 1)&&(_SPI_CPHA == 0)
uint8_t SPI_WRITE_READ_BYTE(uint8_t TX_DAT)
{
    uint8_t i;
    uint8_t RX_DAT = 0;    
       
    for(i = 0;i < 8;i ++)
    {
        SPI_CLK(1);
        
        if(TX_DAT & 0x80)
        {
            SPI_MOSI(1);    
        }
        else
        {
            SPI_MOSI(0);       
        }    

        TX_DAT <<= 1;    
    
        SPI_DELAY(_SPI_DELAY_);
    
        SPI_CLK(0);
       
        RX_DAT <<= 1;
    
        if(SPI_MISO != 0)
        {
            RX_DAT |= 0x01;  			
        }   
		
        SPI_DELAY(_SPI_DELAY_); 		
    }

    SPI_SCK(1);//空闲时SCK为高电平
    
    return RX_DAT;
}
#elif (_SPI_CPOL == 1)&&(_SPI_CPHA == 1)
uint8_t SPI_WRITE_READ_BYTE(uint8_t TX_DAT)
{
    uint8_t i;
    uint8_t RX_DAT = 0;    
       
    for(i = 0;i < 8;i ++)
    {
        SPI_CLK(0);
        
        if(TX_DAT & 0x80)
        {
            SPI_MOSI(1);    
        }
        else
        {
            SPI_MOSI(0);       
        }    

        TX_DAT <<= 1;    
   
        SPI_DELAY(_SPI_DELAY_);
    
        SPI_CLK(1);

        RX_DAT <<= 1;
    
        if(SPI_MISO != 0)
        {
            RX_DAT |= 0x01;    
        }
		
		SPI_DELAY(_SPI_DELAY_);			
			
    }
    return RX_DAT;
}
#endif

void w25qxx_io_writeEnable(void)
{
	SPI_CS(0);
	SPI_WRITE_READ_BYTE(W25X_WriteEnable);//置位WEL,write enable latch;
	SPI_CS(1);
	printf("write enable done...\r\n");
}

void w25qxx_io_waitBusy(void)
{	
	u8 sreg1 =0;
	SPI_CS(0);
	printf("spi_io wait busing... ");
	do{
		SPI_WRITE_READ_BYTE(W25X_ReadStatusReg1);
		sreg1=SPI_WRITE_READ_BYTE(dummy);
		//printf("status register:%2x",sreg1);
	}while(sreg1&0x01);
	SPI_CS(1);
	printf("wait busy done...\r\n");
}


//w25qxx读ID，发送读取ID的指令，然后读取ID
void w25qxx_io_readID(void)
{
	u16 id=0;
	SPI_CS(0);
	SPI_WRITE_READ_BYTE(W25X_ManufactDeviceID);
	SPI_WRITE_READ_BYTE(dummy);
	SPI_WRITE_READ_BYTE(dummy);
	SPI_WRITE_READ_BYTE(0x00);
	id=SPI_WRITE_READ_BYTE(dummy);	//EFh,表示winbone生产
	id=id<<8;
	id|=SPI_WRITE_READ_BYTE(dummy);	//17h,表示w25q128系列
	SPI_CS(1);
	printf("w25qxx id:%4x  \r\n",id);
}

/**一共有4096个sector,每个sector为4096Bytes;************************************
***寻址范围为4096*4096=2^24线；寻址的最小单位是bytes；所以存储容量为2^24*8bit;
***erase sector的寻址参数是sector；
***write page的寻址参数是以Byte为单位的地址；每次最多写入256字节；
***read data的寻址参数也是以Byte为单位的地址，可以一次读完芯片******************/
void w25qxx_io_eraseSector( u16 sector)
{
	u32 address = sector*4096;
	 w25qxx_io_writeEnable( );
	 w25qxx_io_waitBusy();
	 SPI_CS(0);
	 SPI_WRITE_READ_BYTE(W25X_SectorErase);
	 SPI_WRITE_READ_BYTE(address>>16);	//A23-A16
	 SPI_WRITE_READ_BYTE(address>>8);	//A15-A8
	 SPI_WRITE_READ_BYTE(address);		//A7-A0
	 SPI_CS(1);
	 w25qxx_io_waitBusy();
	 printf("erase sector %#x done...\r\n",sector);
}

void w25qxx_io_writePage(u8 *buff,u8 len,u32 address)
{
	u8 i =0;
	w25qxx_io_writeEnable( );
	w25qxx_io_waitBusy();
	SPI_CS(0);
	SPI_WRITE_READ_BYTE(W25X_PageProgram);
	SPI_WRITE_READ_BYTE(address>>16);
	SPI_WRITE_READ_BYTE(address>>8);
	SPI_WRITE_READ_BYTE(address);
	for(i=0;i<len;i++)
		SPI_WRITE_READ_BYTE(buff[i]);
	SPI_CS(1);
	w25qxx_io_waitBusy();
	printf("write page done...\r\n");
}

void w25qxx_io_readData(u8 *buff,u32 len,u32 address)
{
	u32 i =0;
	SPI_CS(0);
	SPI_WRITE_READ_BYTE(W25X_ReadData);
	SPI_WRITE_READ_BYTE(address>>16);
	SPI_WRITE_READ_BYTE(address>>8);
	SPI_WRITE_READ_BYTE(address);
	for(i=0;i<len;i++)
		buff[i]=SPI_WRITE_READ_BYTE(dummy);
	SPI_CS(1);
	printf("read data done...\r\n");
}

void iospi_test(void)
{
	u8 sendbuff[16];
	u8 revbuff[16];
	int i;

	w25qxx_io_readID();
	
	memset(revbuff,0,sizeof(revbuff));
	for(i=0;i<16;i++)
		sendbuff[i]=i;
	
	//对地址4096进行擦除,然后写读数据;	
	w25qxx_io_eraseSector(1);
	w25qxx_io_writePage(sendbuff,16,4096);
	w25qxx_io_readData(revbuff,16,4096);
	for(i=0;i<16;i++)
		printf("revbuff[%2d]:%2x \r\n",i,revbuff[i]);
}






