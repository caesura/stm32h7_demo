/********************************************************************************
*** 适用laser板
*** 需要qspi_bk2外设测试时，在main.h中包含进fpga_qspi_bk2.h文件，
*** 然后调用qspi_bk2_init()和qspi_bk2_test()就可以直接使用了;
***
*** 通过qspi_bk2与fpga通信，
	发送数据为：1byte指令, 3byte地址, 1byte dummy, 1byte数据
	接收数据为：4byte数据 (接收到两个ad9203的实时采样数据)

	本来是使用HAL_QSPI_MspInit()，让qspi初始化的时候自己调用的，
	但是后来代码改成了一个qspi分时复用成两个qspi来使用；
	这样的话要么初始化时同时初始化bk1和bk2，要么不使用回调函数分别初始化；
	根据个人使用习惯，本工程不使用回调函数，然后分别初始化gpio;

*** 延时函数可以使用HAL库自带的HAL_Delay();
*** keil中老是忘记要把xx.c和xx.h文件加载到工程中，然后编译找不到;
********************************************************************************/
#include "fpga_qspi_bk2.h"

QSPI_HandleTypeDef QSPI_bk2_Handler; 

void qspi_bk2_mspinit(QSPI_HandleTypeDef *hqspi)
{
    GPIO_InitTypeDef GPIO_Initure;
    
    __HAL_RCC_QSPI_CLK_ENABLE();        //使能QSPI时钟
    __HAL_RCC_GPIOB_CLK_ENABLE();       //使能GPIOB时钟
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();    

    //****PE7:MISO****PE8:MOSI****PE9:/WP****
    GPIO_Initure.Pin=GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10;
    GPIO_Initure.Mode=GPIO_MODE_AF_PP;         
    GPIO_Initure.Pull=GPIO_PULLUP;              
    GPIO_Initure.Speed=GPIO_SPEED_FREQ_VERY_HIGH; 
    GPIO_Initure.Alternate=GPIO_AF10_QUADSPI;   
    HAL_GPIO_Init(GPIOE,&GPIO_Initure);
    
    //PC11:NCS
    GPIO_Initure.Pin=GPIO_PIN_11;   
	GPIO_Initure.Alternate=GPIO_AF9_QUADSPI; 
    HAL_GPIO_Init(GPIOC,&GPIO_Initure);
    
    //PB2:CLK
    GPIO_Initure.Pin=GPIO_PIN_2;
    HAL_GPIO_Init(GPIOB,&GPIO_Initure);
	printf("qspi_bk2  gpio init done...\r\n");
}

void qspi_bk2_init(void)
{
	qspi_bk2_mspinit(&QSPI_bk2_Handler);
	
    QSPI_bk2_Handler.Instance=QUADSPI;                          //QSPI
    QSPI_bk2_Handler.Init.ClockPrescaler=1;                     //QPSI分频比，频率为100M，200/(1+1)=100MHZ
    QSPI_bk2_Handler.Init.FifoThreshold=32;                      //FIFO阈值为4个字节
    QSPI_bk2_Handler.Init.SampleShifting=QSPI_SAMPLE_SHIFTING_HALFCYCLE;//采样移位半个周期(DDR模式下,必须设置为0)???
    //QSPI_bk2_Handler.Init.FlashSize=POSITION_VAL(0X2000000)-1;  //SPI FLASH大小，W25Q256大小为32M字节
    QSPI_bk2_Handler.Init.FlashSize=15;
    QSPI_bk2_Handler.Init.ChipSelectHighTime=QSPI_CS_HIGH_TIME_5_CYCLE;//片选高电平时间为5个时钟(10*5=55ns),即手册里面的tSHSL参数
    QSPI_bk2_Handler.Init.ClockMode=QSPI_CLOCK_MODE_0;          //模式0
    QSPI_bk2_Handler.Init.FlashID=QSPI_FLASH_ID_2;              //第2片flash
    QSPI_bk2_Handler.Init.DualFlash=QSPI_DUALFLASH_DISABLE;     //禁止双闪存模式
    if(HAL_QSPI_Init(&QSPI_bk2_Handler)==HAL_OK)
		printf("qspi_bk2 init done...\r\n");
    else
		printf("qspi_bk2 init fail...\r\n");   	
}

/*****************************************************************************
	配置格式：指令 + 指令模式 +地址 + 地址模式 + 地址bit数 + dummy周期 + 数据模式
	
			  XX模式：      0线，1线，2线，4线
			  地址bit数：w25q64,w25q128,w25q256的寻址长度是不同的
			  dummy周期：单位为时钟周期; 2个dummy周期为2个无意义的clk；
*****************************************************************************/
void qspi_bk2_send_cmd(u32 instruction,u32 instructionMode,u32 address,u32 addressMode,u32 addressSize,u32 dummyCycles,u32 dataMode)
{
    QSPI_CommandTypeDef Cmdhandler;
    
    Cmdhandler.Instruction=instruction;			//待发指令
    Cmdhandler.InstructionMode=instructionMode;	//指令模式，0线，1线，2线，4线；0线的意思就是不发送的意思；
    														
    Cmdhandler.Address=address;					//待发地址
    Cmdhandler.AddressMode=addressMode;			//地址模式，0线，1线，2线，4线
    Cmdhandler.AddressSize=addressSize;			//地址长度，8bit 16bit 24bit 32bit
    
    Cmdhandler.DummyCycles=dummyCycles;			//待发dummy周期数(dummy是无意义周期，并不是空周期)
    														
    Cmdhandler.DataMode=dataMode;				//数据模式, 0线，1线，2线，4线
    
    Cmdhandler.SIOOMode=QSPI_SIOO_INST_EVERY_CMD;       	//每次都收发数据的时候都带<指令，地址，dummy>作为头格式
    Cmdhandler.AlternateByteMode=QSPI_ALTERNATE_BYTES_NONE; //无交替字节
    Cmdhandler.DdrMode=QSPI_DDR_MODE_DISABLE;           	//关闭DDR模式
    Cmdhandler.DdrHoldHalfCycle=QSPI_DDR_HHC_ANALOG_DELAY;

    if(HAL_QSPI_Command(&QSPI_bk2_Handler,&Cmdhandler,5000)==HAL_OK)
		;//printf("qspi_bk2 send command done...\r\n");
	else
		printf("qspi_bk2 send command fail...\r\n");
}


void qspi_bk2_rev(u8* buf,u32 datalen)
{
    QSPI_bk2_Handler.Instance->DLR=datalen-1;     //配置数据长度
    if(HAL_QSPI_Receive(&QSPI_bk2_Handler,buf,1000)==HAL_OK) 
		;//printf("qspi_bk2 rev done...\r\n");
    else 
		printf("qspi_bk2 rev fail...\r\n");
}


void qspi_bk2_trans(u8* buf,u32 datalen)
{
    QSPI_bk2_Handler.Instance->DLR=datalen-1;    //配置数据长度
    if(HAL_QSPI_Transmit(&QSPI_bk2_Handler,buf,1000)==HAL_OK) 
		;//printf("qspi_bk2 transmit done...\r\n");
    else 
		printf("qspi_bk2 transmit fail...\r\n");
}

void qspi_bk2_deinit(void)
{
	HAL_QSPI_DeInit(&QSPI_bk2_Handler);
	printf("qspi_bk2 deinit done.\r\n");
}


void qspi_bk2_test(void)
{
	u8 senddata[1] = {0x56};
	u8 revdata[8] = {0x00,0x00,0x00,0x00};
	
	/**qspi格式: 1byte指令, 3byte地址, 1byte dummy, 1byte数据*****************/
	qspi_bk2_send_cmd(0x12,QSPI_INSTRUCTION_4_LINES,0x000034,QSPI_ADDRESS_4_LINES,QSPI_ADDRESS_24_BITS,QSPI_BK2_DUMMY_2_CYCLE,QSPI_DATA_4_LINES);
	qspi_bk2_trans(senddata,1);
	qspi_bk2_rev(revdata,4);	
	
	printf("qspi_bk2 revdata: ad2: %#x %#x  ad1: %#x %#x \r\n",revdata[0],revdata[1],revdata[2],revdata[3]);	
}








