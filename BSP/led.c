
/*****************************************************************
*** __NOP(); 为一个机器周期的延时函数
*** HAL_Delay();为HAL库自带的延时函数,单位毫秒

******************************************************************/
#include "led.h"

//初始化PB2为输出.并使能时钟		    
void led_init(void)
{
    GPIO_InitTypeDef GPIO_Initure;
    __HAL_RCC_GPIOB_CLK_ENABLE();					//开启GPIOB时钟
	
    GPIO_Initure.Pin=GPIO_PIN_2;			
    GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  		//推挽输出
    GPIO_Initure.Pull=GPIO_PULLUP;         			//上拉
    GPIO_Initure.Speed=GPIO_SPEED_FREQ_VERY_HIGH;  	//高速
    HAL_GPIO_Init(GPIOB,&GPIO_Initure);     		
}

void led_test(void)
{
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,GPIO_PIN_RESET); //PB2置0	
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,GPIO_PIN_SET);	//PB2置1	
	HAL_Delay(500);
}


