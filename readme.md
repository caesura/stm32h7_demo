      
          
             
	1 USER目录  
		1.1 main.c和main.h               
		1.2 workspace子目录：keil工程文件              
		     
	2 HAL目录
		2.1 Inc目录:xx.h
		2.2 Src目录:xx.c
		2.3 内核配置,启动文件
		    				
	3 BSP目录
		3.1 inc目录：xx.h bsp文件的头文件都放在这里，方便keil一次性包含  
		3.2 xx.c：		  bsp文件的驱动文件都放在这里  
											
	4 使用概括
		使用测试外设代码的话，首先在main.h中包含BSP目录下该外设的xx.h，
		然后在main函数中调用函数即可；
		BSP目录下的每个xx.c文件都是单独工作的外设，彼此互不交叉调用；  

	BSP代码使用说明           							
	1 stm32h7 & cycloneV
		1.1 w25qxx_io.c | w25qxx_spi.c  
			对于w25qxx的测试，2种代码不要混用以防配置冲突，根据功能选一个使用
			iospi_init(); 		//io模拟spi初始化
			iospi_test(); 		//spi测试

			spi1_init();		//spi1外设初始化
			spi1_test();		//spi1外设测试
													
		1.2 fmc.c    
			fmc的异步NOR通讯测试           
			fmc_nor_init();		//fmc_nor_init外设初始化
			fmc_nor_test();		//fmc_nor_test外设测试
							
	2 stm32h7 & cycloneIV
		2.1 w25qxx_qspi.c  
			w25qxx的qspi_bk1通信测试
			qspi_bk1_init();	//qspi_bk1外设初始化
			qspi_bk1_test();	//qspi_bk1外设测试
										
		2.2 fpga_qspi_bk2.c  
			qspi_bk2的qspi通信测试
			qspi_bk2_init();	//qspi_bk2外设初始化
			qspi_bk2_test();	//qspi_bk2外设测试
									
		qspi的双闪存模式是指同时操作两个flash,一部分数据存储在flash1，一部分数据存储在flash2；
		本质上是使用一个qspi外设，将数据存储在两个flash中;这样数据从原先的4线扩展成了8线，又快了1倍；
													
		不使用双闪存模式的话，使用qspi_bk1和qspi_bk2可以分时复用控制两个flash的数据；
		这样就相当于一个qspi外设我们分时复用，变成了两个qspi外设；
		qspi_bk1同flash通信，qspi_bk2同fpga通信；

		ps:其实这里还还可以修改一下，配置成无指令,地址,dummy的通信方式;  
			然后在fpga端，使用ram核来收发数据，相当于使用fpga模拟flash，可能会实用一些，以后再说吧  
											
													

												

           
